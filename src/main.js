'use strict';

import express from 'express';
import http from 'http';
import path from 'path';
import fs from 'fs';
import SocketIO from 'socket.io';
import MongoJs from 'mongojs';
import Multer from 'multer';

// connect now, and worry about collections later
var db = MongoJs('mongodb://localhost/surfriders');
var zones = db.collection('zones');

//Multer
const upload = Multer({
	dest: "../public/uploads"
	// you might also want to set some limits: https://github.com/expressjs/multer#limits
});

const handleError = (err, res) => {
	res
		.status(500)
		.contentType("text/plain")
		.end("Oops! Something went wrong!");
};

let app = express();
let server = http.Server(app);
let io = new SocketIO(server);
let port = process.env.PORT || 3000;

app.set('view engine', 'ejs')
app.use(express.static('public'))

app.use((req, res, next) => {
	res.header("Access-Control-Allow-Origin", "*");
	res.header(
		"Access-Control-Allow-Headers",
		"Origin, X-Requested-With, Content-Type, Accept, Authorization"
	);
	next();
});

app.get("/public/uploads/*.png", (req, res) => {
	res.sendFile(path.join(__dirname, "./uploads/*.png"));
});

app.get('/', (req, res) => {
  res.render('index');
})

app.get('/markers', (req, res) => {
	db.zones.find(function (err, docs) {
		res.send(docs);
	})
})

app.get("/setMarkerToCollect", (req, res) => {
	var markerId = req.query.markerId;
	var ObjectId = MongoJs.ObjectId;

	db.zones.update(
		{ _id: ObjectId(markerId) },
		{ $set: { "infos.collect": true }}
	)

	io.sockets.emit('refreshMarkers');
})

app.post(
	"/upload",
	upload.single("file"),
	(req, res) => {
		const tempPath = req.file.path;
		const targetPath = path.join(__dirname, tempPath + ".png");
		var marker = JSON.parse(req.body.marker);
		marker.infos.picture = "192.168.1.75:3000/uploads/" + req.file.filename + ".png";

		fs.rename(tempPath, targetPath, err => {
			if (err) return handleError(err, res);

			res
				.status(200)
				.contentType("text/plain")
				.end("File uploaded!");
		});

		// find everything
		db.zones.save(marker, function (err, docs) {
			 if (err) {
					console.log(err)
			 } else {
			 		io.sockets.emit('refreshMarkers');
			 }
		 })
	}
);

io.on('connection', (socket) => {
  console.log("Connection");

	var clients = io.sockets.clients(); // This returns an array with all connected clients

  socket.on('addMarker', (data) => {

		io.sockets.emit('refreshMarkers');
  });
});

server.listen(port, () => {
  console.log('[INFO] Listening on *:' + port);
});